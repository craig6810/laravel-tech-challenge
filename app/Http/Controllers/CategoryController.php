<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function __invoke($id)
    {
        $category = Category::find($id);
        $breadcrumbs = DB::table('categories')
            ->where('id', '=', $category->parent_id)
            ->get();

        $breadcrumbs->push($category);

        $categories = DB::table('categories')
            ->where('parent_id', '=', $id)
            ->get();

        $categoriesArray = $categories->pluck('id')->toArray();
        $categoriesArray[] = $id;

        $products = DB::table('products')
            ->whereIn('category_id', $categories->pluck('id')->toArray())
            ->simplePaginate(5);

        return view('categories', [
                'breadcrumbs' => $breadcrumbs,
                'categories' => $categories,
                'products' => $products
            ]
        );
    }
}
