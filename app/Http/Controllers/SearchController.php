<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function __invoke(Request $request)
    {
        $phrase = $request->query('phrase');
        $products = DB::table('products')
            ->where('name', 'like', "{$phrase}%")
            ->simplePaginate(5);

        $products->appends(['phrase' => $phrase]);

        return view('search', [
            'phrase' => $phrase,
            'products' => $products
        ]);
    }
}
