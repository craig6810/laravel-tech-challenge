<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __invoke($id)
    {
        $product = Product::findOrFail($id);
        $breadcrumbs = DB::table('categories')
            ->where('id', '=', $product->category_id)
            ->get();

        foreach ($breadcrumbs as $breadcrumb) {
            if($breadcrumb->parent_id !== null) {
                $parent = Category::find($breadcrumb->parent_id );
                $breadcrumbs->push($parent);
            }
        }

        return view('product', [
                'product' => $product,
                'breadcrumbs' => $breadcrumbs->reverse()
            ]
        );
    }
}
