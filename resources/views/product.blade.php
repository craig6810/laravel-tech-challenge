@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <img src="/images/product_placeholder.jpeg" class="rounded mx-auto d-block">
            </div>

            <div class="col">
                <h3 class="text-center">{{ $product->name }}</h3>

                <p class="font-weight-bold text-center">&pound{{ $product->price }}</p>

                <p class="text-center">
                    orem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.
                </p>

                <button class="btn btn-success float-right">Add to Basket <i class="fas fa-shopping-cart"></i></button>
            </div>
        </div>
    </div>
@endsection