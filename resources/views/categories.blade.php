@extends('layouts.app')

@section('content')
    <div class="text-center">
       @if($categories && count($categories) > 0)
            @foreach($categories as $category)
                <a href="/category/{{ $category->id }}">
                    <span class="badge badge-pill badge-primary w-25 p-2">{{ $category->name  }}</span>
                </a>
            @endforeach
       @endif
    </div>

    <div class="row justify-content-md-center mt-2">
            @foreach ($products as $product)
                <div class="card mr-2 mb-2" style="width: 10rem;">
                    <img src="/images/product_placeholder.jpeg" class="rounded card-img-top img-fluid">
                    <div class="card-body text-center">
                        <p class="card-title">
                            <span><a href="/product/{{ $product->id }}" class="btn btn-light">{{ $product->name }}</a></span>
                        </p>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="row justify-content-md-center">
            {{ $products->links() }}
        </div>
@endsection