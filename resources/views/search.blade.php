@extends('layouts.app')

@section('content')
    <div class="row justify-content-md-center mt-2">
        @if(isset($products) && count($products) === 0)
            <h4>Sorry, no products matched your search <em>'{{ $phrase }}'</em></h4>
        @else
            <h4>The following products matched your search <em>'{{ $phrase }}'</em></h4>
        @endif
    </div>

    @if(isset($products) && count($products) > 0)
        <div class="row justify-content-md-center mt-2">
            @foreach ($products as $product)
                <div class="card mr-2 mb-2" style="width: 10rem;">
                    <img src="/images/product_placeholder.jpeg" class="rounded card-img-top img-fluid">
                    <div class="card-body text-center">
                        <p class="card-title">
                            <span><a href="/product/{{ $product->id }}" class="btn btn-light">{{ $product->name }}</a></span>
                        </p>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="row justify-content-md-center">
            {{ $products->links() }}
        </div>
    @else
        <img src="/images/not-found.png" class="rounded mx-auto d-block">
    @endif
@endsection