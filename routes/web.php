<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;

Route::get('category/{id}', 'CategoryController');

Route::get('product/{id}', 'ProductController');

Route::get('search', 'SearchController');

Route::get('/', function () {
    $categories = DB::table('categories')
        ->whereNull('parent_id')
        ->get();

    $products = DB::table('products')
        ->where('id', '>', '0')
        ->simplePaginate(5);

    return view('categories', [
        'categories' => $categories,
        'products' => $products
    ]);
});

